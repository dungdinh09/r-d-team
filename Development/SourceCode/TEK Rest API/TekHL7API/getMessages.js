

// BEFORE RUNNING:
// ---------------
// 1. If not already done, enable the Cloud Healthcare API
//    and check the quota for your project at
//    https://console.developers.google.com/apis/api/healthcare
// 2. This sample uses Application Default Credentials for authentication.
//    If not already done, install the gcloud CLI from
//    https://cloud.google.com/sdk and run
//    `gcloud beta auth application-default login`.
//    For more information, see
//    https://developers.google.com/identity/protocols/application-default-credentials
// 3. Install the Node.js client library by running
//    `npm install googleapis --save`

const {google} = require('googleapis');
const healthcare = google.healthcare('v1beta1');

async function main () {
  const authClient = await authorize();
  const request = {
    // The resource name of the HL7v2 message to retrieve.
    name: 'projects/centered-style-294707/locations/us-central1/datasets/Tekmedi_DataSet/hl7V2Stores/Tekmedi_datastore/messages/AdEUkhnxGzbMjIM3VNZx8zZxyFkPRa8pSTnjLfEi-uw=',  // TODO: Update placeholder value.

    auth: authClient,
  };

  try {
    const response = (await healthcare.projects.locations.datasets.hl7V2Stores.messages.get(request)).data;
    // TODO: Change code below to process the `response` object:
    console.log(JSON.stringify(response, null, 2));
  } catch (err) {
    console.error(err);
  }
}
main();

async function authorize() {
  const auth = new google.auth.GoogleAuth({
    scopes: ['https://www.googleapis.com/auth/cloud-platform']
  });
  return await auth.getClient();
}