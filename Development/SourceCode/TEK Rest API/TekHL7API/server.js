const {google} = require('googleapis');
const healthcare = google.healthcare('v1');
const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);

const createHl7v2Message = async () => {
  const auth = await google.auth.getClient({
    scopes: ['https://www.googleapis.com/auth/cloud-platform'],
  });
  google.options({auth});

  // TODO(developer): uncomment these lines before running the sample
  const cloudRegion = 'us-central1';
  const projectId = 'centered-style-294707';
  const datasetId = 'Tekmedi_DataSet';
  const hl7v2StoreId = 'Tekmedi_datastore';
  const hl7v2MessageFile = 'hl7v2-message.json';
  const hl7v2Message = JSON.parse(await readFile(hl7v2MessageFile));

  const parent = `projects/${projectId}/locations/${cloudRegion}/datasets/${datasetId}/hl7V2Stores/${hl7v2StoreId}`;
  //const parent = `projects/adjective-noun-123/locations/us-central1/datasets/my-dataset/hl7V2Stores/my-hl7v2-store`;
  const request = {parent, resource: hl7v2Message};

  const response = await healthcare.projects.locations.datasets.hl7V2Stores.messages.create(
    request
  );
  const {data} = response;
  console.log('Created HL7v2 message with data:\n', data);
};

createHl7v2Message();
