const {google} = require('googleapis');
const healthcare = google.healthcare('v1');
const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);

const ingestHl7v2Message = async () => {
  const auth = await google.auth.getClient({
    scopes: ['https://www.googleapis.com/auth/cloud-platform'],
  });
  google.options({auth});

  // TODO(developer): uncomment these lines before running the sample
  const cloudRegion = 'us-central1';
  const projectId = 'centered-style-294707';
  const datasetId = 'Tekmedi_DataSet';
  const hl7v2StoreId = 'Tekmedi_datastore';
  const hl7v2MessageFile = 'hl7v2-message.json';
  const hl7v2Message = JSON.parse(await readFile(hl7v2MessageFile));

  const parent = `projects/${projectId}/locations/${cloudRegion}/datasets/${datasetId}/hl7V2Stores/${hl7v2StoreId}`;
  //const parent = `projects/adjective-noun-123/locations/us-central1/datasets/my-dataset/hl7V2Stores/my-hl7v2-store`;
  const request = {parent, resource: hl7v2Message};

  const response = await healthcare.projects.locations.datasets.hl7V2Stores.messages.ingest(
    request
  );
  const data = response.data.hl7Ack;
  const buff = new Buffer.from(data, 'base64');
  const hl7Ack = buff.toString('ascii');
  console.log('Ingested HL7v2 message with ACK:\n', hl7Ack);
};

ingestHl7v2Message();